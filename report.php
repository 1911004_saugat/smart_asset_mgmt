<?php
  $page_title = 'Generated Report';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
   page_require_level(2);
  $products = join_product_table();
?>
<?php include_once('layouts/header.php'); ?>
  <div class="row">
     <div class="col-md-12">
       <?php echo display_msg($msg); ?>
     </div>
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-body">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th class="text-center" style="width: 50px;">#</th>
                <th> Photo</th>
                <th> Product Title </th>
                <th class="text-center" style="width: 10%;"> Categories </th>
                <th class="text-center" style="width: 10%;"> Instock </th>                
                <th class="text-center" style="width: 10%;"> Unit Value </th>
                <th class="text-center" style="width: 10%;"> Total Value </th>
                
              </tr>
            </thead>
            <tbody>
              <?php foreach ($products as $product):?>
              <tr>
                <td class="text-center"><?php echo count_id();?></td>
                <td>
                  <?php if($product['media_id'] === '0'): ?>
                    <img class="img-avatar img-circle" src="uploads/products/no_image.jpg" alt="">
                  <?php else: ?>
                  <img class="img-avatar img-circle" src="uploads/products/<?php echo $product['image']; ?>" alt="">
                <?php endif; ?>
                </td>
                <td> <?php echo remove_junk($product['name']); ?></td>
                <td class="text-center"> <?php echo remove_junk($product['categorie']); ?></td>
                <td class="text-center"> <?php echo remove_junk($product['quantity']); ?></td>
      
                <td class="text-center"> <?php echo remove_junk($product['sale_price']); ?></td>
                <td class="text-center"> <?php echo remove_junk($product['sale_price']*$product['quantity']); ?></td>
        
            
              </tr>
             <?php endforeach; ?>
            </tbody>
            <tfoot>
    <tr>
        <td colspan="4"></td>
        <td class="text-center">
            <strong>Total Quantity:</strong>
            <?php 
            $total_quantity = 0;
            foreach ($products as $product) {
                $total_quantity += $product['quantity'];
            }
            echo $total_quantity;
            ?>
        </td>
        <td></td>
        <td class="text-center">
            <strong>Total Value:</strong>
            $<?php 
            $total_value = 0;
            foreach ($products as $product) {
                $total_value += $product['sale_price'] * $product['quantity'];
            }
            echo number_format($total_value, 2);
            ?>
        </td>
    </tr>
</tfoot>
          </tabel>
        </div>
      </div>
    </div>
  </div>
  <?php include_once('layouts/footer.php'); ?>
