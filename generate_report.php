<?php
require_once('../common/lib.php');
require_once('../lib/class.db.php');
require_once('../common/config.php');

$db = new DB();

//get the start and end date for the report
$start_date = $_POST['start_date'];
$end_date = $_POST['end_date'];

//query to select all assets within the given date range
$query = "SELECT * FROM assets WHERE date_acquired >= '$start_date' AND date_acquired <= '$end_date'";
$assets = $db->select($query);

//calculate the total value of assets
$total_value = 0;
foreach($assets as $asset){
    $total_value += $asset['value'];
}

//generate the report
echo "Asset Management System Report<br><br>";
echo "Date Range: $start_date to $end_date<br><br>";
echo "Total Number of Assets: ".count($assets)."<br>";
echo "Total Value of Assets: $".$total_value."<br><br>";

echo "<table border='1'>";
echo "<tr>";
echo "<th>Asset ID</th>";
echo "<th>Asset Name</th>";
echo "<th>Asset Type</th>";
echo "<th>Date Acquired</th>";
echo "<th>Value</th>";
echo "</tr>";

foreach($assets as $asset){
    echo "<tr>";
    echo "<td>".$asset['id']."</td>";
    echo "<td>".$asset['name']."</td>";
    echo "<td>".$asset['type']."</td>";
    echo "<td>".$asset['date_acquired']."</td>";
    echo "<td>$".$asset['value']."</td>";
    echo "</tr>";
}

echo "</table>";

?>